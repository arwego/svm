# SVM #


Show the result of classification using SVM

### Breast Cancer ###
Source: UCI / Wisconsin Breast Cancer
Preprocessing: Note that the original data has the column 1 containing sample ID. Also 16 instances with missing values are removed.
* of classes: 2
* of data: 683
* of features: 10

### diabetes ###
Source: UCI / Pima Indians Diabetes
* of classes: 2
* of data: 768
* of features: 8

### heart ###
Source: Statlog / Heart
* of classes: 2
* of data: 270
* of features: 13