#!/usr/bin/env python

import os
import urllib
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets

def main():

    '''
    #  Attribute                     Domain
    --------------------------------------------
    1. Sample code number            id number
    2. Clump Thickness               1 - 10
    3. Uniformity of Cell Size       1 - 10
    4. Uniformity of Cell Shape      1 - 10
    5. Marginal Adhesion             1 - 10
    6. Single Epithelial Cell Size   1 - 10
    7. Bare Nuclei                   1 - 10
    8. Bland Chromatin               1 - 10
    9. Normal Nucleoli               1 - 10
    10. Mitoses                      1 - 10
    11. Class:                       (2 for benign, 4 for malignant)
    '''

    csv = 'breast-cancer-wisconsin.csv'
    dataset = np.loadtxt (csv, delimiter=",")
    # print(dataset.shape)

    for k in range(1,9):
        print('Using feature: ' + str(k+1) + ' and ' + str(k+2))
        X = dataset[:,k:k+2]
        y = dataset[:,10]

        h = .02
        C = 1.0
        svc = svm.SVC(kernel='linear', C=C).fit(X, y)
        rbf_svc = svm.SVC(kernel='rbf', gamma=0.7, C=C).fit(X, y)
        poly_svc = svm.SVC(kernel='poly', degree=2, C=C).fit(X, y)
        poly3_svc = svm.SVC(kernel='poly', degree=3, C=C).fit(X, y)

        x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
        y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))

        titles = ['SVC with linear kernel',
                  'SVC with RBF kernel',
                  'SVC with polynomial d=2 kernel',
                  'SVC with polynomial d=3 kernel']

        for i, clf in enumerate((svc, rbf_svc, poly_svc, poly3_svc)):
            plt.subplot(2, 2, i + 1)
            plt.subplots_adjust(wspace=0.4, hspace=0.4)

            Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
            Z = Z.reshape(xx.shape)
            plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)
            plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.coolwarm)
            xname = 'Feature ' + str(k+1)
            yname = 'Feature ' + str(k+2)
            plt.xlabel(xname)
            plt.ylabel(yname)
            plt.xlim(xx.min(), xx.max())
            plt.ylim(yy.min(), yy.max())
            plt.xticks(())
            plt.yticks(())
            plt.title(titles[i])

        # plt.show()
        figname = 'breast_cancer_' + 'feature_' + str(k+1) + '_and_feature_' + str(k+2)
        plt.savefig(figname)
        plt.clf()

if __name__ == '__main__':
    main()
