#!/usr/bin/env python

import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm, datasets

def main():

    '''
    For Each Attribute: (all numeric-valued)
    1. Number of times pregnant
    2. Plasma glucose concentration a 2 hours in an oral glucose tolerance test
    3. Diastolic blood pressure (mm Hg)
    4. Triceps skin fold thickness (mm)
    5. 2-Hour serum insulin (mu U/ml)
    6. Body mass index (weight in kg/(height in m)^2)
    7. Diabetes pedigree function
    8. Age (years)
    9. Class variable (0 or 1)
    '''

    csv = 'pima-indians-diabetes.csv'
    dataset = np.loadtxt (csv, delimiter=",")
    print(dataset.shape)

    for k in range(0,7):
        print('Using feature: ' + str(k+1) + ' and ' + str(k+2))
        X = dataset[:,k:k+2]
        y = dataset[:,8]

        h = .02
        C = 1.0
        svc = svm.SVC(kernel='linear', C=C).fit(X, y)
        rbf_svc = svm.SVC(kernel='rbf', gamma=0.7, C=C).fit(X, y)
        poly_svc = svm.SVC(kernel='poly', degree=2, C=C).fit(X, y)
        poly3_svc = svm.SVC(kernel='poly', degree=3, C=C).fit(X, y)

        x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
        y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))

        titles = ['SVC with linear kernel',
                  'SVC with RBF kernel',
                  'SVC with polynomial d=2 kernel',
                  'SVC with polynomial d=3 kernel']

        for i, clf in enumerate((svc, rbf_svc, poly_svc, poly3_svc)):
            plt.subplot(2, 2, i + 1)
            plt.subplots_adjust(wspace=0.4, hspace=0.4)

            Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
            Z = Z.reshape(xx.shape)
            plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)
            plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.coolwarm)
            xname = 'Feature ' + str(k+1)
            yname = 'Feature ' + str(k+2)
            plt.xlabel(xname)
            plt.ylabel(yname)
            plt.xlim(xx.min(), xx.max())
            plt.ylim(yy.min(), yy.max())
            plt.xticks(())
            plt.yticks(())
            plt.title(titles[i])

        # plt.show()
        figname = 'diabetes_' + 'feature_' + str(k+1) + '_and_feature_' + str(k+2)
        plt.savefig(figname)
        plt.clf()

if __name__ == '__main__':
    main()
